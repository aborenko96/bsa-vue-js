<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Http\Resources\UserResource;
use App\User;
use Illuminate\Http\File;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    public function index()
    {
        return UserResource::collection(User::all());
    }

    public function store(UserRequest $request)
    {
        $file = $request->file('avatar');
        $pathName = $file->getPathname();
        $originalName = $file->getClientOriginalName();

        $savePath = "public/images";

        Storage::putFileAs($savePath, new File($pathName), $originalName);

        $user = User::create([
           'name' => $request->name,
           'email' => $request->email,
           'avatar' => $originalName,
           'password' => ''
        ]);

        return new UserResource($user);
    }

    public function show(User $user)
    {
        return new UserResource($user);
    }

    public function update(UserRequest $request, User $user)
    {
        $file = $request->file('avatar');
        $pathName = $file->getPathname();
        $originalName = $file->getClientOriginalName();

        $savePath = "public/images";

        Storage::putFileAs($savePath, new File($pathName), $originalName);

        $user->update([
            'name' => $request->name,
            'email' => $request->email,
            'avatar' => $originalName,
        ]);

        return new UserResource($user);
    }

    public function destroy(User $user)
    {
        if($user->delete()){
            return new JsonResponse([
                'success' => true
            ]);
        }

        return new JsonResponse([
            'success' => false
        ]);
    }
}
