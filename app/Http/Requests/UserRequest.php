<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Arr;

class UserRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $id = Arr::get($this->request->all(), 'id');

        return [
            'name' => 'required',
            'email' => 'email|unique:users,email' . ($id ? ",$id":""),
            'avatar' => 'required|image'
        ];
    }
}
